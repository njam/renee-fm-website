[www.renee.fm](https://www.renee.fm)
============
Website for *RENÉE* bar's radio station.


About
-----
The dynamic content for the website is stored in a [Directus CMS instance](https://directus-renee-u21722.vm.elestio.app/),
and gets injected into the HTML templates at build-time. The website is re-built after any content changes.


Development
-----------

Build and test the website locally (http://localhost:1235/index.html):
```
yarn run serve
```

If needed you can also create the production-build in `public/`:
```
yarn run build
```


Production Deployment
---------------------

The website is hosted on https://vercel.com/njam/renee-fm-website.

A deployment is _automatically_ triggered when:
- Content is changed in the CMS (via [Directus flow-webhook](https://directus-renee-u21722.vm.elestio.app/admin/settings/flows)).
- Code changes are pushed to Gitlab (via [Vercel integration](https://vercel.com/njam/renee-fm-website)).
- Every 6 hours (via [periodic Gitlab CI job](https://gitlab.com/njam/renee-fm-website/pipeline_schedules)).

To _manually_ deploy to Vercel from local code:
```
yarn run deploy
```
