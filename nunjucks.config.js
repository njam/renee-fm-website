const moment = require('moment-timezone')
const fetch = require('node-fetch')

module.exports = {
  root: './src',
  data: getData
}

async function getData (_pathInfo) {
  let api = new ApiClient('https://directus-renee-u21722.vm.elestio.app', 'Europe/Zurich', 6)
  let events = await api.getEvents()
  let homepage = await api.getHomepage()

  return {
    events,
    homepage,
  }
}

class ApiClient {
  url
  timeZone
  dayOffset

  constructor (url, timeZone, dayOffset) {
    this.url = url
    this.timeZone = timeZone
    this.dayOffset = dayOffset
  }

  async getEvents () {
    let dateMin = moment().tz(this.timeZone)
      .subtract(this.dayOffset, 'hours')
      .set({'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0})
      .add(this.dayOffset, 'hours')
      .toISOString()
    let response = await this._get(`/items/radio_shows?filter[start][_gte]=${dateMin}&sort=start`)

    return response.data.map((event) => {
      let start = moment.tz(event['start'], this.timeZone)
      let start_text;
      if (start.format('H') < this.dayOffset) {
        start.subtract(1, 'day')
        start_text = start.format('dd [night], D MMM H:mm');
      } else {
        start_text = start.format('dd, D MMM H:mm');
      }
      return {
        start_text,
        start_iso: start.toISOString(true),
        title: event['title'],
      }
    })
  }

  async getHomepage () {
    let response = await this._get('/items/website_reneefm?single=1')
    return {
      html_title: response.data['html_title'],
      html_description: response.data['html_description'],
      html_keywords: response.data['html_keywords'],
      intro_text: response.data['intro_text'],
      agenda_title: response.data['agenda_title'],
      imprint: response.data['imprint'],
      google_tag_id: 'G-ZX9RCZE8BY',
    }
  }

  async _get (path) {
    let url = this.url + path
    let response = await fetch(url, {method: 'get'})
    let response_json = await response.json();
    let response_errors = response_json['errors']
    if (response_errors) {
      throw new Error(`API error: ${JSON.stringify(response_errors)}`)
    }
    return response_json
  }
}
